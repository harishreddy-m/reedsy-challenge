### Projects

I started learning Nodejs as hobby while working on java at work. This was five years ago.

My commercial project is Fieldrepo at raksan. We chose meteor framework and worked on it. At this company, it was completely javascript eco system. I learned and taught ES6 concepts, Angular. I improved my nodejs knowledge as I worked on asset tracking feature. This feature is about tracking a mobile location and displaying it on a map in frontend.

[Trippko](http://trippko.netlify.com) is another project I am currently working on using Angular 5 with ngrx for state management.

### Documents versioning

While choosing a solution for solving this problem, one has to choose storage vs computation.

Since the problem specifies to conside storage efficiency, the following solution is going to have more computations than the brute-force solution.

My solution would be creating a schema for time series data.

data tuple consists of
timestamp, line number, operation(ADD|DELETE|UPDATE), newValue, oldValue

By iterating over this collection, it is possible to construct the state of document at any point in time.It is possible to implement all features mentioned in question.


 


